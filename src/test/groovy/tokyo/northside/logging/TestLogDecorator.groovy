package tokyo.northside.logging

import ch.qos.logback.classic.Level
import ch.qos.logback.classic.LoggerContext
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions
import org.slf4j.ILoggerFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.Marker
import org.slf4j.MarkerFactory

import java.util.function.Supplier

class TestLogDecorator {

    static MemoryAppender memoryAppender
    static Logger logger = LoggerFactory.getLogger(TestLogDecorator.class)
    static ILogger newLogger = LoggerDecorator.deco(logger, LogUtil.getBUNDLE())

    @BeforeAll
    static void preUp() {
        memoryAppender = new MemoryAppender()
        memoryAppender.setContext((LoggerContext) LoggerFactory.getILoggerFactory())
        (logger as ch.qos.logback.classic.Logger).addAppender(memoryAppender)
        memoryAppender.start()
    }

    @BeforeEach
    void setup() {
        memoryAppender.reset()
    }

    @Test
    void testGeneralUse() {
        logger.atInfo().log("Simple default template '{}'", "info")
        Assertions.assertTrue(memoryAppender.contains("Simple default template 'info'", Level.INFO))
    }

    @Test
    void testDecorator() {
        LoggerDecorator.deco(logger.atInfo()).log("Simple message.")
        LoggerDecorator.deco(logger.atInfo()).log("Simple JUL style template {0}", "arg")
        LoggerDecorator.deco(logger.atWarn()).log(() -> "Simple default template 'warn'")
        LoggerDecorator.deco(logger.atError()).log("Two arguments: {0} {1}", "one", "two")
        LoggerDecorator.deco(logger.atError()).log("Many arguments: {0} {1} of {2}", "many", "number", "placeholders")
        Assertions.assertTrue(memoryAppender.contains("Simple message.", Level.INFO))
        Assertions.assertTrue(memoryAppender.contains("Simple JUL style template arg", Level.INFO))
        Assertions.assertTrue(memoryAppender.contains("Simple default template 'warn'", Level.WARN))
        Assertions.assertTrue(memoryAppender.contains("Two arguments: one two", Level.ERROR))
        Assertions.assertTrue(memoryAppender.contains("Many arguments: many number of placeholders", Level.ERROR))
    }

    @Test
    void testBundle() {
        LogUtil.deco(logger.atWarn()).setMessageRB("MSG").addArgument("utility").addArgument("LogUtil").log()
        Assertions.assertEquals(1, memoryAppender.countEventsForLogger(TestLogDecorator.class.getName()))
        Assertions.assertTrue(memoryAppender.contains("JUL style template with utility class LogUtil", Level.WARN))
        Assertions.assertTrue(memoryAppender.contains("(MSG)", Level.WARN))
        Assertions.assertEquals("MSG", memoryAppender.getLoggedEvents().get(0).getMDCPropertyMap()
                .get(LoggerDecorator.LOCALISATION_MDC_KEY))
    }

    @Test
    void testException() {
        Throwable t = new Exception("Cause")
        LogUtil.deco(logger.atError()).setMessageRB("NO_PLACEHOLDER").setCause(t).log()
        Assertions.assertEquals(1, memoryAppender.getSize())
        Assertions.assertEquals("Cause", memoryAppender.getLoggedEvents().get(0).getThrowableProxy().getMessage())
    }

    @Test
    void testLogRBArgsException() {
        Throwable t = new Exception("Cause")
        LogUtil.deco(logger.atError()).setCause(t).logRB("MANY_PLACEHOLDERS", "A", "B", "C")
        Assertions.assertEquals(1, memoryAppender.getSize())
        Assertions.assertEquals("Cause", memoryAppender.getLoggedEvents().get(0).getThrowableProxy().getMessage())
    }

    @Test
    void testMarker() {
        Marker m = MarkerFactory.getDetachedMarker("Marker")
        LoggerDecorator.deco(logger.atInfo()).setMessage("Check marker").addMarker(m).log()
        Assertions.assertTrue(memoryAppender.contains("Check marker", Level.INFO))
        Assertions.assertTrue(memoryAppender.getLoggedEvents().get(0).getMarkerList().get(0).contains("Marker"))
    }

    @Test
    void testSupplier() {
        LogUtil.deco(logger.atWarn()).setMessageRB("MSG").addArgument("utility").addArgument(() -> getFqcn(LogUtil.class)).log()
        Assertions.assertTrue(memoryAppender.contains("JUL style template with utility class " + LogUtil.class.getName(), Level.WARN))
    }

    @Test
    void testLogRB() {
        LogUtil.deco(logger.atError()).logRB("NO_PLACEHOLDER")
        LogUtil.deco(logger.atWarn()).logRB("MSG", "test", "TestLocalization")
        LogUtil.deco(logger.atInfo()).logRB("MSG_ONE", "utility")
        LogUtil.deco(logger.atInfo()).logRB("MANY_PLACEHOLDERS", "many", "number of placeholder is", "3")
        Assertions.assertTrue(memoryAppender.contains("Here is no placeholder", Level.ERROR))
        Assertions.assertTrue(memoryAppender.contains("JUL style template with test class TestLocalization", Level.WARN))
        Assertions.assertTrue(memoryAppender.contains("JUL style template utility", Level.INFO))
        Assertions.assertTrue(memoryAppender.contains("There are many placeholders: number of placeholder is 3", Level.INFO))
    }

    @Test
    void testLogKeyValue() {
        LogUtil.deco(logger.atWarn()).setMessageRB("MSG").addArgument("utility").addArgument("LogUtil").addKeyValue("KEY0", "VALUE0").log()
        Assertions.assertTrue(memoryAppender.contains("JUL style template with utility class LogUtil", Level.WARN))
        Assertions.assertEquals("VALUE0", memoryAppender.getLoggedEvents().get(0).getKeyValuePairs().get(0).value)
    }

    static String getFqcn(Class<?> clazz) {
        return clazz.getName()
    }
    
    class SupplierWithBool implements Supplier<Object> {
        public boolean wasCalled = false
            
        Object get() {
            wasCalled = true
            return "sample"
        }
    }
        
    /** Test that supplier is indeed called when log is not generated **/
    @Test
    void testNullLogger() {
        setLogLevelInfo()
        Assertions.assertFalse(logger.isDebugEnabled())
        SupplierWithBool s = new SupplierWithBool()
        LoggingEventDecorator leb = LoggerDecorator.deco(logger.atDebug())
        leb.setMessage("Test {0}")
        leb.addArgument(s)
        leb.log()
        Assertions.assertFalse(s.wasCalled)
    }

    void setLogLevelInfo() {
        ILoggerFactory loggerFactory = LoggerFactory.getILoggerFactory()
        Class<? extends ILoggerFactory> loggerFactoryClass = loggerFactory.getClass()
        String loggerName = loggerFactoryClass.getName()
        if (loggerName.equals("ch.qos.logback.classic.LoggerContext")) {
            ((ch.qos.logback.classic.Logger) logger).setLevel(Level.INFO)
        } else if (loggerName.equals("org.slf4j.jul.JDK14LoggerFactory")) {
            java.util.logging.Logger rootLogger = LogManager.getLogManager().getLogger("")
            rootLogger.setLevel(java.util.logging.Level.INFO)
        }
    }

    @Test
    void testLoggerLegacy() {
        newLogger.error("Simple default template '{0}'", "error")
        Assertions.assertEquals(1, memoryAppender.countEventsForLogger(TestLogDecorator.class.getName()))
        Assertions.assertTrue(memoryAppender.contains("Simple default template 'error'", Level.ERROR))
    }

    @Test
    void testLoggerUsageFluent() {
        newLogger.atInfo().log("Simple default template '{0}'", "info")
        Assertions.assertEquals(1, memoryAppender.countEventsForLogger(TestLogDecorator.class.getName()))
        Assertions.assertTrue(memoryAppender.contains("Simple default template 'info'", Level.INFO))
    }

    @Test
    void testLoggerBundleFluent() {
        newLogger.atWarn().setMessageRB("MSG_ONE").addArgument("with Decorator").log()
        Assertions.assertEquals(1, memoryAppender.countEventsForLogger(TestLogDecorator.class.getName()))
        Assertions.assertTrue(memoryAppender.contains("JUL style template with Decorator", Level.WARN))
        Assertions.assertEquals("MSG_ONE", memoryAppender.getLoggedEvents().get(0).getMDCPropertyMap()
                .get(LoggerDecorator.LOCALISATION_MDC_KEY))
        Assertions.assertTrue(memoryAppender.contains("(MSG_ONE)", Level.WARN))
    }
}
