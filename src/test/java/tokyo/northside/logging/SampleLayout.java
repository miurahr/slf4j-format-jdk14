package tokyo.northside.logging;

import ch.qos.logback.classic.pattern.ThrowableProxyConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.IThrowableProxy;
import ch.qos.logback.core.CoreConstants;
import ch.qos.logback.core.LayoutBase;
import ch.qos.logback.core.util.CachingDateFormatter;
import java.util.List;
import java.util.Map;
import org.slf4j.event.KeyValuePair;

public class SampleLayout extends LayoutBase<ILoggingEvent> {
    ThrowableProxyConverter tpc = new ThrowableProxyConverter();
    CachingDateFormatter cachingDateFormatter = new CachingDateFormatter("YYYY:MM:dd HH:mm:ss.SSS");

    @Override
    public void start() {
        tpc.start();
        super.start();
    }

    @Override
    public String doLayout(ILoggingEvent event) {
        if (!isStarted()) {
            return CoreConstants.EMPTY_STRING;
        }
        StringBuilder sb = new StringBuilder(128);
        sb.append(cachingDateFormatter.format(event.getTimeStamp()));
        sb.append(" ").append(event.getLevel());
        kvp(event, sb);
        sb.append(" - ").append(event.getFormattedMessage());
        Map<String, String> prop = event.getMDCPropertyMap();
        Object key = prop.get("slf4j.localisation.bundle.key");
        if (key != null) {
            sb.append(" (").append(key).append(")");
        }
        sb.append(CoreConstants.LINE_SEPARATOR);
        IThrowableProxy tp = event.getThrowableProxy();
        if (tp != null) {
            String stackTrace = tpc.convert(event);
            sb.append(stackTrace);
        }
        return sb.toString();
    }

    static final char DOUBLE_QUOTE_CHAR = '"';

    private void kvp(ILoggingEvent event, StringBuilder sb) {
        List<KeyValuePair> kvpList = event.getKeyValuePairs();
        if (kvpList == null || kvpList.isEmpty()) {
            return;
        }
        for (KeyValuePair kvp : kvpList) {
            sb.append(' ');
            sb.append(kvp.key);
            sb.append('=');
            sb.append(DOUBLE_QUOTE_CHAR);
            sb.append(kvp.value);
            sb.append(DOUBLE_QUOTE_CHAR);
        }
    }
}
