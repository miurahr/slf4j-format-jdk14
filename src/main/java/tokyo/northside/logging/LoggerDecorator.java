/*
 *  Copyright (C) 2023 Hiroshi Miura
 *
 *  This is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  It is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package tokyo.northside.logging;

import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.event.Level;
import org.slf4j.spi.LoggingEventBuilder;
import org.slf4j.spi.NOPLoggingEventBuilder;

public class LoggerDecorator implements ILogger {

    private final Logger logger;
    private final ResourceBundle bundle;

    /**
     * A key of MDC slf4j-format-jdk14 automatically adds a value,
     * when a user specifies a resource bundle key for a localized
     * log message.
     */
    public static final String LOCALISATION_MDC_KEY = "slf4j.localisation.bundle.key";

    /**
     * System property key to be set "true" when a user wants to
     * slf4j-format-jdk14 append a resource bundle key in a message.
     */
    public static final String LOCALISATION_KEY_APPENDER = "slf4j.localisation.bundle.key.appender";

    /**
     * org.slf4j.Logger decorator which accepts JUL style message format.
     * @param logger org.slf4j.Logger object.
     * @return ILogger instance.
     */
    @SuppressWarnings("unused")
    public static ILogger deco(Logger logger) {
        return new LoggerDecorator(logger, null);
    }

    /**
     * org.slf4j.Logger decorator which accepts JUL style message format.
     * @param logger org.slf4j.Logger object.
     * @param bundle resource bundle for localization.
     * @return ILogger instance.
     */
    public static ILogger deco(Logger logger, ResourceBundle bundle) {
        return new LoggerDecorator(logger, bundle);
    }

    /**
     * Stream style logger decorator implementation, which accepts JUL style
     * format based on SLF4J.
     *
     * @param builder
     *            SLF4J LoggingEventBuilder, which return by atLevel(level) or
     *            its variants.
     * @return LoggerDecorator object.
     */
    public static LoggingEventDecorator deco(LoggingEventBuilder builder) {
        if (builder instanceof NOPLoggingEventBuilder) {
            return LoggingEventDecoratorPassive.INSTANCE;
        } else {
            // For the moment, create a new one. We can later use a cache if necessary
            return new LoggingEventDecoratorActive(builder);
        }
    }

    /**
     * Stream style logger decorator implementation, which accepts JUL style
     * format based on SLF4J.
     * @param builder
     *            SLF4J LoggingEventBuilder, which return by atLevel(level) or
     *            its variants.
     * @param bundle
     *            Resource Bundle
     * @return LoggerDecorator object.
     */
    public static LoggingEventDecorator deco(LoggingEventBuilder builder, ResourceBundle bundle) {
        if (builder instanceof NOPLoggingEventBuilder) {
            return LoggingEventDecoratorPassive.INSTANCE;
        } else {
            // For the moment, create a new one. We can later use a cache if necessary
            return new LoggingEventDecoratorActive(builder, bundle);
        }
    }

    public LoggerDecorator(Logger logger, ResourceBundle bundle) {
        this.logger = logger;
        this.bundle = bundle;
    }

    @Override
    public String getName() {
        return logger.getName();
    }

    public LoggingEventDecorator makeLoggingEventBuilder(Level level) {
        if (bundle != null) {
            return LoggerDecorator.deco(logger.atLevel(level), bundle);
        } else {
            return LoggerDecorator.deco(logger.atLevel(level));
        }
    }

    @Override
    public boolean isTraceEnabled() {
        return logger.isTraceEnabled();
    }

    @Override
    public void trace(String msg) {
        logger.trace(msg);
    }

    @Override
    public void trace(String format, Object arg) {
        makeLoggingEventBuilder(Level.TRACE).log(format, arg);
    }

    @Override
    public void trace(String format, Object arg1, Object arg2) {
        makeLoggingEventBuilder(Level.TRACE).log(format, arg1, arg2);
    }

    @Override
    public void trace(String format, Object... arguments) {
        makeLoggingEventBuilder(Level.TRACE).log(format, arguments);
    }

    @Override
    public void trace(String msg, Throwable t) {
        makeLoggingEventBuilder(Level.TRACE).setCause(t).setMessage(msg).log();
    }

    @Override
    public boolean isTraceEnabled(Marker marker) {
        return logger.isTraceEnabled(marker);
    }

    @Override
    public void trace(Marker marker, String msg) {
        makeLoggingEventBuilder(Level.TRACE).addMarker(marker).setMessage(msg).log();
    }

    @Override
    public void trace(Marker marker, String format, Object arg) {
        makeLoggingEventBuilder(Level.TRACE)
                .addMarker(marker)
                .setMessage(format)
                .addArgument(arg)
                .log();
    }

    @Override
    public void trace(Marker marker, String format, Object arg1, Object arg2) {
        makeLoggingEventBuilder(Level.TRACE)
                .addMarker(marker)
                .setMessage(format)
                .addArgument(arg1)
                .addArgument(arg2)
                .log();
    }

    @Override
    public void trace(Marker marker, String format, Object... argArray) {
        makeLoggingEventBuilder(Level.TRACE).addMarker(marker).log(format, argArray);
    }

    @Override
    public void trace(Marker marker, String msg, Throwable t) {
        makeLoggingEventBuilder(Level.TRACE)
                .addMarker(marker)
                .setCause(t)
                .setMessage(msg)
                .log();
    }

    @Override
    public boolean isDebugEnabled() {
        return logger.isDebugEnabled();
    }

    @Override
    public void debug(String msg) {
        logger.debug(msg);
    }

    @Override
    public void debug(String format, Object arg) {
        makeLoggingEventBuilder(Level.DEBUG).log(format, arg);
    }

    @Override
    public void debug(String format, Object arg1, Object arg2) {
        makeLoggingEventBuilder(Level.DEBUG).log(format, arg1, arg2);
    }

    @Override
    public void debug(String format, Object... arguments) {
        makeLoggingEventBuilder(Level.DEBUG).log(format, arguments);
    }

    @Override
    public void debug(String msg, Throwable t) {
        makeLoggingEventBuilder(Level.DEBUG).setCause(t).setMessage(msg).log();
    }

    @Override
    public boolean isDebugEnabled(Marker marker) {
        return logger.isDebugEnabled(marker);
    }

    @Override
    public void debug(Marker marker, String msg) {
        makeLoggingEventBuilder(Level.DEBUG).addMarker(marker).setMessage(msg).log();
    }

    @Override
    public void debug(Marker marker, String format, Object arg) {
        makeLoggingEventBuilder(Level.DEBUG)
                .addMarker(marker)
                .setMessage(format)
                .addArgument(arg)
                .log();
    }

    @Override
    public void debug(Marker marker, String format, Object arg1, Object arg2) {
        makeLoggingEventBuilder(Level.DEBUG)
                .addMarker(marker)
                .setMessage(format)
                .addArgument(arg1)
                .addArgument(arg2)
                .log();
    }

    @Override
    public void debug(Marker marker, String format, Object... arguments) {
        makeLoggingEventBuilder(Level.DEBUG).addMarker(marker).log(format, arguments);
    }

    @Override
    public void debug(Marker marker, String msg, Throwable t) {
        makeLoggingEventBuilder(Level.DEBUG)
                .addMarker(marker)
                .setCause(t)
                .setMessage(msg)
                .log();
    }

    @Override
    public boolean isInfoEnabled() {
        return logger.isInfoEnabled();
    }

    @Override
    public void info(String msg) {
        logger.info(msg);
    }

    @Override
    public void info(String format, Object arg) {
        makeLoggingEventBuilder(Level.INFO).log(format, arg);
    }

    @Override
    public void info(String format, Object arg1, Object arg2) {
        makeLoggingEventBuilder(Level.INFO).log(format, arg1, arg2);
    }

    @Override
    public void info(String format, Object... arguments) {
        makeLoggingEventBuilder(Level.INFO).log(format, arguments);
    }

    @Override
    public void info(String msg, Throwable t) {
        makeLoggingEventBuilder(Level.INFO).setCause(t).setMessage(msg).log();
    }

    @Override
    public boolean isInfoEnabled(Marker marker) {
        return logger.isInfoEnabled(marker);
    }

    @Override
    public void info(Marker marker, String msg) {
        makeLoggingEventBuilder(Level.INFO).addMarker(marker).log(msg);
    }

    @Override
    public void info(Marker marker, String format, Object arg) {
        makeLoggingEventBuilder(Level.INFO).addMarker(marker).log(format, arg);
    }

    @Override
    public void info(Marker marker, String format, Object arg1, Object arg2) {
        makeLoggingEventBuilder(Level.INFO).addMarker(marker).log(format, arg1, arg2);
    }

    @Override
    public void info(Marker marker, String format, Object... arguments) {
        makeLoggingEventBuilder(Level.INFO).addMarker(marker).log(format, arguments);
    }

    @Override
    public void info(Marker marker, String msg, Throwable t) {
        makeLoggingEventBuilder(Level.INFO)
                .addMarker(marker)
                .setCause(t)
                .setMessage(msg)
                .log();
    }

    @Override
    public boolean isWarnEnabled() {
        return logger.isWarnEnabled();
    }

    @Override
    public void warn(String msg) {
        makeLoggingEventBuilder(Level.WARN).log(msg);
    }

    @Override
    public void warn(String format, Object arg) {
        makeLoggingEventBuilder(Level.WARN).log(format, arg);
    }

    @Override
    public void warn(String format, Object... arguments) {
        makeLoggingEventBuilder(Level.WARN).log(format, arguments);
    }

    @Override
    public void warn(String format, Object arg1, Object arg2) {
        makeLoggingEventBuilder(Level.WARN).log(format, arg1, arg2);
    }

    @Override
    public void warn(String msg, Throwable t) {
        makeLoggingEventBuilder(Level.WARN).setCause(t).setMessage(msg).log();
    }

    @Override
    public boolean isWarnEnabled(Marker marker) {
        return logger.isWarnEnabled(marker);
    }

    @Override
    public void warn(Marker marker, String msg) {
        makeLoggingEventBuilder(Level.WARN).addMarker(marker).log(msg);
    }

    @Override
    public void warn(Marker marker, String format, Object arg) {
        makeLoggingEventBuilder(Level.WARN).addMarker(marker).log(format, arg);
    }

    @Override
    public void warn(Marker marker, String format, Object arg1, Object arg2) {
        makeLoggingEventBuilder(Level.WARN).addMarker(marker).log(format, arg1, arg2);
    }

    @Override
    public void warn(Marker marker, String format, Object... arguments) {
        makeLoggingEventBuilder(Level.WARN).addMarker(marker).log(format, arguments);
    }

    @Override
    public void warn(Marker marker, String msg, Throwable t) {
        makeLoggingEventBuilder(Level.WARN)
                .addMarker(marker)
                .setCause(t)
                .setMessage(msg)
                .log();
    }

    @Override
    public boolean isErrorEnabled() {
        return logger.isErrorEnabled();
    }

    @Override
    public void error(String msg) {
        logger.error(msg);
    }

    @Override
    public void error(String format, Object arg) {
        makeLoggingEventBuilder(Level.ERROR).log(format, arg);
    }

    @Override
    public void error(String format, Object arg1, Object arg2) {
        makeLoggingEventBuilder(Level.ERROR).log(format, arg1, arg2);
    }

    @Override
    public void error(String format, Object... arguments) {
        makeLoggingEventBuilder(Level.ERROR).log(format, arguments);
    }

    @Override
    public void error(String msg, Throwable t) {
        makeLoggingEventBuilder(Level.ERROR).setCause(t).setMessage(msg).log();
    }

    @Override
    public boolean isErrorEnabled(Marker marker) {
        return logger.isErrorEnabled(marker);
    }

    @Override
    public void error(Marker marker, String msg) {
        makeLoggingEventBuilder(Level.ERROR).addMarker(marker).log(msg);
    }

    @Override
    public void error(Marker marker, String format, Object arg) {
        makeLoggingEventBuilder(Level.ERROR).addMarker(marker).log(format, arg);
    }

    @Override
    public void error(Marker marker, String format, Object arg1, Object arg2) {
        makeLoggingEventBuilder(Level.ERROR).addMarker(marker).log(format, arg1, arg2);
    }

    @Override
    public void error(Marker marker, String format, Object... arguments) {
        makeLoggingEventBuilder(Level.ERROR).addMarker(marker).log(format, arguments);
    }

    @Override
    public void error(Marker marker, String msg, Throwable t) {
        makeLoggingEventBuilder(Level.ERROR)
                .addMarker(marker)
                .setCause(t)
                .setMessage(msg)
                .log();
    }
}
