/*
 *  Copyright (C) 2023 Hiroshi Miura
 *
 *  This is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  It is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package tokyo.northside.logging;

import java.util.ResourceBundle;

@SuppressWarnings("unused")
public final class LoggerFactory {
    private LoggerFactory() {}

    public static final String ROOT_LOGGER_NAME = org.slf4j.Logger.ROOT_LOGGER_NAME;

    public static ILogger getLogger(Class<?> clazz, ResourceBundle bundle) {
        return new LoggerDecorator(org.slf4j.LoggerFactory.getLogger(clazz), bundle);
    }

    public static ILogger getLogger(String name, ResourceBundle bundle) {
        return new LoggerDecorator(org.slf4j.LoggerFactory.getLogger(name), bundle);
    }

    public static ILogger getLogger(Class<?> clazz) {
        return new LoggerDecorator(org.slf4j.LoggerFactory.getLogger(clazz), null);
    }

    public static ILogger getLogger(String name) {
        return new LoggerDecorator(org.slf4j.LoggerFactory.getLogger(name), null);
    }
}
