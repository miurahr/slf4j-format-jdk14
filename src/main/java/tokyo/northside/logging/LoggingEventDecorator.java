/*
 *  Copyright (C) 2023 Hiroshi Miura
 *
 *  This is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  It is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package tokyo.northside.logging;

import java.util.function.Supplier;
import org.slf4j.Marker;
import org.slf4j.spi.LoggingEventBuilder;

public interface LoggingEventDecorator extends LoggingEventBuilder {

    /**
     * Set the cause for the logging event being built.
     * @param cause a throwable
     * @return AbstractLoggerDecorator
     */
    @Override
    LoggingEventDecorator setCause(Throwable cause);

    /**
     * A marker to the event being built.
     * @param marker a Marker instance to add.
     * @return AbstractLoggerDecorator
     */
    @Override
    LoggingEventDecorator addMarker(Marker marker);

    /**
     * Add an argument to the event being built.
     * @param p an Object to add.
     * @return AbstractLoggerDecorator
     */
    @Override
    LoggingEventDecorator addArgument(Object p);

    /**
     * Add an argument supplier to the event being built.
     * @param objectSupplier an Object supplier to add.
     * @return AbstractLoggerDecorator
     */
    @Override
    LoggingEventDecorator addArgument(Supplier<?> objectSupplier);

    /**
     * Add a key value pair to the event being built.
     * @param key the key of the key value pair.
     * @param value the value of the key value pair.
     * @return AbstractLoggerDecorator
     */
    @Override
    LoggingEventDecorator addKeyValue(String key, Object value);

    /**
     * Add a key value pair to the event being built.
     * @param key the key of the key value pair.
     * @param valueSupplier a supplier of a value for the key value pair.
     * @return AbstractLoggerDecorator
     */
    @Override
    LoggingEventDecorator addKeyValue(String key, Supplier<Object> valueSupplier);

    /**
     * Sets the message of the logging event.
     * @param message log message.
     * @return AbstractLoggerDecorator
     */
    @Override
    LoggingEventDecorator setMessage(String message);

    /**
     * Sets the message of the event via a message supplier.
     * @param messageSupplier supplies a String to be used as the message for the event
     * @return AbstractLoggerDecorator
     */
    @Override
    LoggingEventDecorator setMessage(Supplier<String> messageSupplier);

    /**
     * Set Message from a key of a resource bundle.
     * @param message log message.
     * @return AbstractLoggerDecorator
     */
    LoggingEventDecorator setMessageRB(String message);

    /**
     * Equivalent to calling setMessageRB(String) followed by
     * addArgument(Object) and then log().
     * @param key
     *            message key in a resource bundle.
     */
    void logRB(String key);

    /**
     * Equivalent to calling setMessageRB(String) followed by
     * addArgument(Object) and then log().
     * @param key
     *            message key in a resource bundle.
     * @param arg
     *            first argument to be used with the message to log
     */
    void logRB(String key, Object arg);

    /**
     * Equivalent to calling setMessageRB(String) followed by two calls of
     * addArgument(Object) and then log().
     * @param key
     *            message key in a resource bundle.
     * @param arg0
     *            first argument to be used with the message to log
     * @param arg1
     *            second argument to be used with the message to log
     */
    void logRB(String key, Object arg0, Object arg1);

    /**
     * Equivalent to calling setMessage(String) followed by zero or more calls
     * to addArgument(Object).
     * (depending on the size of args array) and then log().
     * @param key
     *            message key in a resource bundle.
     * @param args
     *            a list (actually an array) of arguments to be used with the message to log
     */
    void logRB(String key, Object... args);
}
