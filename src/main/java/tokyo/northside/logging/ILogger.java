/*
 *  Copyright (C) 2023 Hiroshi Miura
 *
 *  This is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  It is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package tokyo.northside.logging;

import org.slf4j.Logger;
import org.slf4j.event.Level;
import org.slf4j.spi.DefaultLoggingEventBuilder;

public interface ILogger extends Logger {

    default LoggingEventDecorator makeLoggingEventBuilder(Level level) {
        return LoggerDecorator.deco(new DefaultLoggingEventBuilder(this, level));
    }

    default LoggingEventDecorator atLevel(Level level) {
        if (isEnabledForLevel(level)) {
            return makeLoggingEventBuilder(level);
        } else {
            return new LoggingEventDecoratorPassive();
        }
    }

    default LoggingEventDecorator atTrace() {
        if (isTraceEnabled()) {
            return makeLoggingEventBuilder(Level.TRACE);
        } else {
            return new LoggingEventDecoratorPassive();
        }
    }

    default LoggingEventDecorator atDebug() {
        if (isDebugEnabled()) {
            return makeLoggingEventBuilder(Level.DEBUG);
        } else {
            return new LoggingEventDecoratorPassive();
        }
    }

    default LoggingEventDecorator atInfo() {
        if (isInfoEnabled()) {
            return makeLoggingEventBuilder(Level.INFO);
        } else {
            return new LoggingEventDecoratorPassive();
        }
    }

    default LoggingEventDecorator atWarn() {
        if (isWarnEnabled()) {
            return makeLoggingEventBuilder(Level.WARN);
        } else {
            return new LoggingEventDecoratorPassive();
        }
    }

    default LoggingEventDecorator atError() {
        if (isErrorEnabled()) {
            return makeLoggingEventBuilder(Level.ERROR);
        } else {
            return new LoggingEventDecoratorPassive();
        }
    }
}
