/*
 *  Copyright (C) 2023 Hiroshi Miura, Thomas Cordonnier
 *                2015 Aaron Madlon-Kay, OmegaT team
 *
 *  This is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  It is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package tokyo.northside.logging;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.function.Supplier;
import org.slf4j.MDC;
import org.slf4j.Marker;
import org.slf4j.event.KeyValuePair;
import org.slf4j.spi.CallerBoundaryAware;
import org.slf4j.spi.LoggingEventBuilder;

/**
 * Decorator for event builders which really do something
 * @author Hiroshi Miura
 */
class LoggingEventDecoratorActive implements LoggingEventDecorator, CallerBoundaryAware {
    private String message;
    private final ResourceBundle bundle;
    private String key;
    private Supplier<String> messageSupplier;
    private Throwable exception;
    private List<Object> arguments;
    private List<Marker> markers;
    private List<KeyValuePair> keyValuePairs;

    private final LoggingEventBuilder loggingEventBuilder;
    private final boolean appender;
    private static final String FQCN = LoggingEventDecoratorActive.class.getName();
    protected String fqcn = FQCN;

    /**
     * Stream style logger decorator implementation, which accepts JUL style
     * format based on SLF4J.
     *
     * @param builder
     *            SLF4J LoggingEventBuilder, which return by atLevel(level) or
     *            its variants.
     * @param bundle
     *            ResourceBundle to localize log messages. null when not localize.
     * @param appender
     *            true if a user wants to add logger to append "(KEY)" to a localized
     *            message, in which KEY is bundle key passed.
     *            Default is "false".
     *            When a user sets a system property which key is defined as
     *            tokyo.northside.logging.LoggerDecorator.LOCALISATION_KEY_APPENDER
     *            set to "TRUE" or "true", it will be "true" at default.
     */
    public LoggingEventDecoratorActive(LoggingEventBuilder builder, ResourceBundle bundle, boolean appender) {
        this.loggingEventBuilder = builder;
        this.bundle = bundle;
        this.appender = appender;
    }

    /**
     * Stream style logger decorator implementation, which accepts JUL style
     * format based on SLF4J.
     * When a user sets a system property which key is defined as
     * tokyo.northside.logging.LoggerDecorator.LOCALISATION_KEY_APPENDER
     * set to "TRUE" or "true", appender will be "true" at default.
     *
     * @param builder
     *            SLF4J LoggingEventBuilder, which return by atLevel(level) or
     *            its variants.
     * @param bundle
     *            ResourceBundle to localize log messages. null when not localize.
     */
    public LoggingEventDecoratorActive(LoggingEventBuilder builder, ResourceBundle bundle) {
        this(builder, bundle, isAppender());
    }

    /**
     * Stream style logger decorator implementation, which accepts JUL style
     * format based on SLF4J.
     *
     * @param builder
     *            SLF4J LoggingEventBuilder, which return by atLevel(level) or
     *            its variants.
     */
    public LoggingEventDecoratorActive(LoggingEventBuilder builder) {
        this(builder, null, false);
    }

    private static boolean isAppender() {
        return "true".equalsIgnoreCase(System.getProperty(LoggerDecorator.LOCALISATION_KEY_APPENDER));
    }

    /**
     * {@inheritDoc}
     */
    public LoggingEventDecorator setMessage(String message) {
        this.message = message;
        this.key = null;
        this.messageSupplier = null;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    public LoggingEventDecorator setMessageRB(String key) {
        this.key = key;
        this.messageSupplier = null;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LoggingEventDecorator setCause(Throwable cause) {
        exception = cause;
        return this;
    }

    @Override
    public LoggingEventDecorator addMarker(final Marker marker) {
        if (markers == null) {
            markers = new ArrayList<>(2);
        }
        markers.add(marker);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LoggingEventDecorator addArgument(Object p) {
        getNonNullArguments().add(p);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LoggingEventDecorator addArgument(Supplier<?> objectSupplier) {
        addArgument(objectSupplier.get());
        return this;
    }

    @Override
    public LoggingEventDecorator addKeyValue(final String key, final Object value) {
        getNonnullKeyValuePairs().add(new KeyValuePair(key, value));
        return this;
    }

    @Override
    public LoggingEventDecorator addKeyValue(final String key, final Supplier<Object> valueSupplier) {
        getNonnullKeyValuePairs().add(new KeyValuePair(key, valueSupplier));
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LoggingEventDecorator setMessage(final Supplier<String> messageSupplier) {
        this.messageSupplier = messageSupplier;
        this.key = null;
        return this;
    }

    /**
     * publish log message.
     */
    @Override
    public void log() {
        performLog();
    }

    private void performLog() {
        LoggingEventBuilder leb = loggingEventBuilder;
        if (key != null && bundle != null) {
            MDC.put(LoggerDecorator.LOCALISATION_MDC_KEY, key);
            String msg = format(getResourceStringOrKey(bundle, key), getArgumentArray());
            if (appender) {
                leb = leb.setMessage(msg + " (" + key + ")");
            } else {
                leb = leb.setMessage(msg);
            }
        } else if (messageSupplier != null) {
            leb = leb.setMessage(format(messageSupplier.get(), getArgumentArray()));
        } else if (message != null) {
            leb = leb.setMessage(format(message, getArgumentArray()));
        }
        if (exception != null) {
            leb = leb.setCause(exception);
        }
        if (markers != null) {
            for (Marker m : markers) {
                leb = leb.addMarker(m);
            }
        }
        if (keyValuePairs != null) {
            for (KeyValuePair p : keyValuePairs) {
                leb = leb.addKeyValue(p.key, p.value);
            }
        }
        if (leb instanceof CallerBoundaryAware) {
            ((CallerBoundaryAware) leb).setCallerBoundary(fqcn);
        }
        leb.log();
        if (MDC.get(LoggerDecorator.LOCALISATION_MDC_KEY) != null) {
            MDC.remove(LoggerDecorator.LOCALISATION_MDC_KEY);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void log(final String message) {
        setMessage(message).log();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void log(final String message, final Object arg) {
        setMessage(message).addArgument(arg).log();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void log(final String message, final Object arg0, final Object arg1) {
        setMessage(message).addArgument(arg0).addArgument(arg1).log();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void log(final String message, final Object... args) {
        getNonNullArguments().addAll(Arrays.asList(args));
        setMessage(message);
        performLog();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void log(final Supplier<String> messageSupplier) {
        setMessage(messageSupplier).log();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void logRB(String key) {
        setMessageRB(key).log();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void logRB(String key, Object arg) {
        setMessageRB(key).addArgument(arg).log();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void logRB(String key, Object arg0, Object arg1) {
        setMessageRB(key).addArgument(arg0).addArgument(arg1).log();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void logRB(String key, Object... args) {
        getNonNullArguments().addAll(Arrays.asList(args));
        setMessageRB(key);
        performLog();
    }

    private List<Object> getNonNullArguments() {
        if (arguments == null) {
            arguments = new ArrayList<>(3);
        }
        return arguments;
    }

    private List<KeyValuePair> getNonnullKeyValuePairs() {
        if (keyValuePairs == null) {
            keyValuePairs = new ArrayList<>(4);
        }
        return keyValuePairs;
    }

    private Object[] getArgumentArray() {
        if (arguments == null) {
            return null;
        }
        return arguments.toArray();
    }

    /**
     * Formats J.U.L message strings.
     * <p>
     * Note: This is only a first attempt at putting right what goes wrong in
     * MessageFormat. Currently, it only duplicates single quotes, but it doesn't
     * even test if the string contains parameters (numbers in curly braces),
     * and it doesn't allow for string contains already escaped quotes.
     *
     * @param str
     *            The string to format
     * @param arguments
     *            Arguments to use in formatting the string
     *
     * @return The formatted string
     */
    private String format(String str, Object... arguments) {
        // MessageFormat.format expects single quotes to be escaped
        // by duplicating them, otherwise the string will not be formatted
        if (arguments != null && arguments.length > 0) {
            String format = str.replaceAll("'", "''");
            try {
                return MessageFormat.format(format, arguments);
            } catch (IllegalArgumentException iae) {
                // default to the same behavior as in
                // java.util.logging.Formatter.formatMessage(LogRecord)
                return str;
            }
        }
        return str;
    }

    /* Borrowed from slf4j-jdk-platform-logging:o.s.j.p.l.SLF4JPlatformLogger */
    private static String getResourceStringOrKey(ResourceBundle bundle, String key) {
        if (bundle == null || key == null) return key;
        // ResourceBundle::getString throws:
        //
        // * NullPointerException for null keys
        // * ClassCastException if the message is no string
        // * MissingResourceException if there is no message for the key
        //
        // Handle all of these cases here to avoid log-related exceptions from crashing the JVM.
        try {
            return bundle.getString(key);
        } catch (MissingResourceException ex) {
            return key;
        } catch (ClassCastException ex) {
            return bundle.getObject(key).toString();
        }
    }

    @Override
    public void setCallerBoundary(String fqcn) {
        this.fqcn = fqcn;
    }
}
