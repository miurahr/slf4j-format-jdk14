# Classes and methods

Here is a belief description of core API. 

## `LoggerDecorator` class

`LoggerDecorator` is a main class of a slf4j-format-jdk14 library.

There is a function, `LoggerDecorator#deco` takes `org.slf4j.Logger` instance and `ResourceBundle` object.
A returned object implements `ILogger` interface which behaves as mostly similar as `org.slf4j.Logger`,
but supports template placeholder like '{0}' and localization with ResourceBundle.

To localize messages, you need to use Fluent API and new methods `setMessageRB` or `logRB`.

The function `LoggerDecorator#deco` also takes object that implements `LoggingEventBuilder`,
typically uses with `logger#atLevel`, or `logger#atInfo` etc.

When `LoggerDecorator#deco` takes `LoggingEventBuilder` object only, `logRB` and `setMessageRB` does not produce
localized messages but returns empty string. It is even useful when you want to use a format syntax which
placeholder is like `{0}`, `LoggingEventDecorator` handle it through `setMessage` and `log` methods.

It is also another signature `deco(LoggingEventBuilder, ResourceBundle)` that will support localization of messages.

## `tokyo.northside.logging.LoggerFactory` class

This is a class to provide syntax sugar for users who use SLF4J's `LoggerFactory.getLogger(clazz)` method.
When you call `tokyo.northside.logging.ILogger logger = tokyo.northside.logging.LoggerFactory.getLogger(clazz, BUNDLE);`
then you can enjoy compatibles functions with SLF4J.

You don't need to change other lines if you have already satisfied logging in your class.

And it also provides features `slf4j-format-jdk14` provide.
Your `logger.info("message {0}", arg);` handle JUL style placeholder.

You can use a resource bundle with `logger.atInfo().setMessageRB("RESOURCE_BUNDLE_KEY").log()`.

## `tokyo.northside.logging.LoggerDecorator` implements `tokyo.northside.logging.ILogger` methods

`tokyo.northside.logging.ILogger` is extended from `org.slf4j.Logger` interface.
It makes `LoggerDecorator` compatible with `org.slf4j.Logger` as a decorator.

There are 6 methods, `atLevel`, `atInfo`, `atError`, `atWarn`, `atDebug`, `atTrace` that returns
`tokyo.northside.logging.LoggingEventDecorator` instance.

With this contract, `LoggerDecorator` provide a message format and l10n functionality.

## `tokyo.northside.logging.LoggingEventDecorator` interface

`tokyo.northside.logging.LoggingEventDecorator` interface extends `org.slf4j.spi.LoggingEventBuilder` interface.
When user has `logger` object created by `LoggerFactory.getLogger()` function,
the object implement a `LoggingEventBuiler` interface is returned when user calls `logger#atLevel()` method.

`LoggingEventDecoder` interface is for the object that is decorated by `LoggerDecorator#deco` function.
It is guaranteed to be compatible with `LoggingEventBuilder` interface, so user can use same methods
 SLF4J API provide.

`LoggingEventDecoder` interface introduce new two methods with several argument signatures.

### `setMessageRB` method

`setMessageRB` method accept a KEY for resource bundle and set message from a localized text.
The method will return an object implements `LoggingEvnetDecorator`, typically returns `this`.  

### `logRB` method

`logRB` method accept a KEY for resource bundle, and use a message from a localized text,
and behave similar as `log` method.

## `LoggingEventDecoratorActive` and `LoggingEventDecoratorPassive` class

These are concrete implementations which implements `LoggingEventDecorater` and `CallerBoundaryAware` interfaces.
