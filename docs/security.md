# Security Policy

## Supported Versions

| Version | Status              |
|---------|---------------------|
| 1.0.x   | Stable version      |
| 0.5.0   | Development version |
| < 0.5   | not supported       |

## Reporting a Vulnerability

Please disclose security vulnerabilities privately at miurahr@linux.com
