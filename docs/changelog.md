# Change Log
All notable changes to this project will be documented in this file.

## [Unreleased]

## [v0.5.1]
* Chore: Use version catalog in gradle
* Bump Gradle@8.3

## [v0.5.0]
* Introduce system property to indicate the library to append "(KEY)" to message text(#20)
* add LoggerFactory syntax (#19)
* Add bundle key in MDC when localized (#16)
* Test custom configuration with LogBack (#16)
* Introduce ILogger interface that extends org.slf4j.Logger (#13)
* Support decorator for org.slf4j.Logger (#13)
* Change internal class and interface names (#12)
* Feature: support CallerBoundaryAware (#7)
* Update manual page

## [v0.4.0]
* Change interface to abstract class to support mixture of legacy and new method (#5)

## [v0.3.1]
* Fix javadoc (#4)

## [v0.3.0]
* Do nothing when used with a non-active SLF4J logger (NOPLoggingEventBuilder) (#1)
* Improve test coverage (#2)
* Add a manual page on readthedocs.org (#3)

## [v0.2.3]
* Fix CI/CD release configuration

## [v0.2.0]
* Publish to OSSRH
* CI/CD on ci.codeberg.org

## v0.1.0
* First internal release

[Unreleased]: https://codeberg.org/miurahr/slf4j-format-jdk14/compare/v0.5.1...HEAD
[v0.5.1]: https://codeberg.org/miurahr/slf4j-format-jdk14/compare/v0.5.0...v0.5.1
[v0.5.0]: https://codeberg.org/miurahr/slf4j-format-jdk14/compare/v0.4.0...v0.5.0
[v0.4.0]: https://codeberg.org/miurahr/slf4j-format-jdk14/compare/v0.3.1...v0.4.0
[v0.3.1]: https://codeberg.org/miurahr/slf4j-format-jdk14/compare/v0.3.0...v0.3.1
[v0.3.0]: https://codeberg.org/miurahr/slf4j-format-jdk14/compare/v0.2.3...v0.3.0
[v0.2.3]: https://codeberg.org/miurahr/slf4j-format-jdk14/compare/v0.2.0...v0.2.3
[v0.2.0]: https://codeberg.org/miurahr/slf4j-format-jdk14/compare/v0.1.0...v0.2.0
