# Welcome to SLF4j-format-jdk14

Decorator for SLF4J format extension to support `java.util.logging` style message format and
localization with a resource bundle.

## How to install

`slf4j-format-jdk14` is published at Maven Central.

When your project uses Gradle, you can specify it like as follows.

```groovy
dependencies {
    implementation 'tokyo.northside:slf4j-format-jdk14:<VERSION>'
}
```

Please check a [Maven Central repository](https://central.sonatype.com/artifact/tokyo.northside/slf4j-format-jdk14/) for details.

## Dependency

`slf4j-format-jdk14` depends on `slf4j-api` version `2.0.7`.
It is compatible with Java 11 and later.


## How to use

`slf4j-format-jdk14` provide a class of [`Decorator` design pattern](https://en.wikipedia.org/wiki/Decorator_pattern).
It wraps `org.slf4j.Logger` object, and `LoggingEventBuilder` object which the `org.slf4j.Logger` instance returns
when calling `atLevel(Level)` method.
Decorated object support all the methods which supported by SLF4J `Logger` provide.
It also provides additional methods `logRB` and `setMessageRB` which accept a key of `ResourceBundle` for localization.

### Usage with `tokyo.northside.logging.LoggerFactory`

`slf4j-format-jdk14` provide `tokyo.northside.logging.LoggerFactory` class that can be used as similar as
genuine `org.slf4j.LoggerFactory`.

After create `tokyo.northside.logging.ILogger` instance, you can use it as same as slf4j Logger and
enjoy extension `setMessageRB` method that accept ResourceBundle key.

```java
import tokyo.northside.logging.ILogger;
import tokyo.northside.logging.LoggerFactory;

class Main {
    public static void example() {
        ResourceBundle bundle = ResourceBundle.getBundle("org/example/Bundle");
        ILogger logger = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME, bundle);

        try {
            logger.atInfo().log("JUL style format {0}", "argument");
        } catch (Exception ex) {
            // get format string from bundle
            logger.atError().setMessageRB("ERROR_MESSAGE_KEY").setCuase(ex).log();
        }
    }
}
```

## Log message localization

You can use `setMessageRB(key)` method to specify a key of the specified resource bundle.
A log message is going to be like "INFO: Localized message 'arg' (MSG)" in the following example.

Bundle.properties

```properties
MSG=Localized message '{0}'
```

You can use styles both `SetMessageRB` and `logRB`. Both methods recognize passed string as
a bundle key instead of a message template.

```java
import tokyo.northside.logging.ILogger;
import tokyo.northside.logging.LoggerFactory;

class Example {
    private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("Bundle");
    
    public static void example() {
        ILogger logger = LoggerFactory.getLogger(Example.class, BUNDLE);
   
        logger.atInfo().setMessageRB("MSG").addArgument("arg").log();
        logger.atInfo().logRB("MSG", "arg");
    }
}
```
