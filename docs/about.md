# About author

Hiroshi Miura is an author of `sllf4j-format-jdk14` library.
He is known as a developer of many free software libraries in Python and Java.
You can reach him at the following links;

- Blog: [northside tokyo](https://blogs.northside.tokyo/)
- Github: [@miurahr](https://github.com/miurahr)
- LinkedIn: [Hiroshi Miura](https://linkedin.com/in/miurahr)
- OpenHub: [OpenHUB person](https://openhub.net/accounts/miurahr)
- Mastodon: [@miurahr@en.osm.town](https://en.osm.town/@miurahr)
- Mastodon: [@miurahr@floss.social](https://floss.social/@miurahr)
