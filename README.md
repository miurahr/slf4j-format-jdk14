# slf4j-format-jdk14

Decorator for SLF4J format extension to support `java.util.logging` style message format and
localization with a resource bundle.

## Status

A project is considered as BETA status.

## How to install

`slf4j-format-jdk14` is published at Maven Central.

When your project uses Gradle, you can specify it like as follows.

```groovy
dependencies {
    implementation 'tokyo.northside:slf4j-format-jdk14:<VERSION>'
}
```

Please check a [Maven Central repository](https://central.sonatype.com/artifact/tokyo.northside/slf4j-format-jdk14/) for details.

## Dependency

`slf4j-format-jdk14` depends on `slf4j-api` version `2.0.7`.
It is compatible with Java 11 and later.


## How to use

`slf4j-format-jdk14` provide a class of [`Decorator` design pattern](https://en.wikipedia.org/wiki/Decorator_pattern)
which wraps `LoggingEventBuilder` object that `Logger` instance create when calling `atLevel(Level)` method.
Decorated object support all the methods which supported by SLF4J `Logger` provide.
It also provides additional methods `logRB` and `setMessageRB` which accept a key of `ResourceBundle` for localization.

Here is an example of how to use.

```java
import tokyo.northside.logging.ILogger;
import tokyo.northside.logging.LoggerFactory;

class Main {
    public static void example() {
        final ResourceBundle bundle = ResourceBundle.getBundle("org/example/Bundle");
        ILogger logger = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME, bundle);

        try {
            // Support '{0}' style template placeholder
            logger.atInfo().log("JUL style format {0}", "argument");
            logger.info("{0} style logging", "Legacy");

            // Localization with `setMessageRB` and `logRB`
            logger.atInfo().logRB("BUNDLE_KEY");
            logger.atInfo().setMessageRB("BUNDLE_KEY").log();

            // some code which may throw exception here.

        } catch (Exception ex) {
            // get format string from bundle
            logger.atError().setMessageRB("ERROR_MESSAGE_KEY").setCuase(ex).log();
        }
    }
}
```

Please see a detail at [Manual page at Read the Docs](https://slf4j-format-jdk14.readthedocs.org/)

`slf4j-format-jdk14` depends on "Fluent logging API" of SLF4j.
Please check [SLF4J manual - Fluent logging API](https://www.slf4j.org/manual.html#fluent) for details.

## License

A library is distributed under GNU General public license version 3.0 or later.
