import java.io.FileInputStream
import java.util.Properties

plugins {
    groovy
    jacoco
    signing
    `java-library`
    `java-library-distribution`
    `maven-publish`
    alias(libs.plugins.spotbugs)
    alias(libs.plugins.spotless)
    alias(libs.plugins.git.version) apply false
    alias(libs.plugins.nexus.publish)
}

// we handle cases without .git directory
val dotgit = project.file(".git")
if (dotgit.exists()) {
    apply(plugin = libs.plugins.git.version.get().pluginId)
    val versionDetails: groovy.lang.Closure<com.palantir.gradle.gitversion.VersionDetails> by extra
    val details = versionDetails()
    val baseVersion = details.lastTag.substring(1)
    version = when {
        details.isCleanTag -> baseVersion
        else -> baseVersion + "-" + details.commitDistance + "-" + details.gitHash + "-SNAPSHOT"
    }
} else {
    val gitArchival = project.file(".git-archival.properties")
    val props = Properties()
    props.load(FileInputStream(gitArchival))
    val versionDescribe = props.getProperty("describe")
    val regex = "^v\\d+\\.\\d+\\.\\d+$".toRegex()
    version = when {
        regex.matches(versionDescribe) -> versionDescribe.substring(1)
        else -> versionDescribe.substring(1) + "-SNAPSHOT"
    }
}

publishing {
    publications {
        create<MavenPublication>("mavenJava") {
            from(components["java"])
            pom {
                name.set("slf4j-format-jdk14")
                description.set("JUL style format decorator for SLF4J")
                url.set("https://codeberg.org/miurahr/slf4j-format-jdk14")
                licenses {
                    license {
                        name.set("The GNU General Public License, Version 3")
                        url.set("https://www.gnu.org/licenses/licenses/gpl-3.html")
                        distribution.set("repo")
                    }
                }
                developers {
                    developer {
                        id.set("miurahr")
                        name.set("Hiroshi Miura")
                        email.set("miurahr@linux.com")
                    }
                }
                scm {
                    connection.set("scm:git:git://codeberg.org/miurahr/slf4j-format-jdk14.git")
                    developerConnection.set("scm:git:git://codeberg.org/miurahr/slf4j-format-jdk14.git")
                    url.set("https://codeberg.org/miurahr/sld4j-foramt-jdk14")
                }
            }
        }
    }
}

val signKey = listOf("signingKey", "signing.keyId", "signing.gnupg.keyName").find {project.hasProperty(it)}
tasks.withType<Sign> {
    onlyIf { signKey !=null && !project.version.toString().endsWith("-SNAPSHOT") }
}

signing {
    when (signKey) {
        "signingKey" -> {
            val signingKey: String? by project
            val signingPassword: String? by project
            useInMemoryPgpKeys(signingKey, signingPassword)
        }
        "signing.keyId" -> {/* do nothing */}
        "signing.gnupg.keyName" -> {
            useGpgCmd()
        }
    }
    sign(publishing.publications["mavenJava"])
}
nexusPublishing.repositories {
    sonatype()
}

tasks.jar {
    manifest {
        attributes("Automatic-Module-Name" to "slf4j.format.jdk14")
    }
}

group = "tokyo.northside"

repositories {
    mavenCentral()
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(11))
    }
    withSourcesJar()
    withJavadocJar()
}

dependencies {
    implementation(libs.slf4j.api)
    testImplementation(libs.groovy.core)
    testImplementation(libs.junit.jupiter)
    testImplementation(libs.logback.classic)
}

tasks.spotbugsMain {
    excludeFilter.set(project.file("config/spotbugs/exclude.xml"))
    reports.create("html") {
        required.set(true)
        outputLocation.set(project.layout.buildDirectory.file("reports/spotbugs.html").get().asFile)
        setStylesheet("fancy-hist.xsl")
    }
}

tasks.spotbugsTest {
    enabled = false
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}

tasks.withType<JavaCompile> {
    options.compilerArgs.add("-Xlint:deprecation")
    options.compilerArgs.add("-Xlint:unchecked")
}

tasks.jacocoTestReport {
    dependsOn(tasks.test)
    reports {
        html.required = true
    }
}

spotless {
    format("misc") {
        target(listOf("*.gradle", ".gitignore", "*.md", "*.rst"))
        trimTrailingWhitespace()
        indentWithSpaces()
        endWithNewline()
    }
    java {
        target(listOf("src/*/java/**/*.java"))
        palantirJavaFormat()
        importOrder()
        removeUnusedImports()
        formatAnnotations()
    }
}
